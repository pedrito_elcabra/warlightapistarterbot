﻿namespace WarlightApiStarterBot.Services.Constants
{
    public static class ApiConstants
    {
        public const string CREATE_GAME = "CreateGame";
        public const string GAME_FEED = "GameFeed";
    }
}
