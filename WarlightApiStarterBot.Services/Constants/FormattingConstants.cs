﻿namespace WarlightApiStarterBot.Services.Constants
{
    public static class FormattingConstants
    {
        public const string APPEND_URL_FORMAT = "{0}/{1}";
        public const string APPEND_QUERY_FORMAT = "{0}?{1}={2}";
    }
}
