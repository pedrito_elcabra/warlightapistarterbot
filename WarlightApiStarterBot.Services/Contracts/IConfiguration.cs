﻿namespace WarlightApiStarterBot.Services.Contracts
{
    public interface IConfiguration
    {
        string ApiKey { get; }
    }
}
