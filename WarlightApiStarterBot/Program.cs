﻿using Ninject;

namespace WarlightApiStarterBot
{
    class Program
    {
        static void Main(string[] args)
        {
            IKernel kernel = new StandardKernel();
            kernel.Load("WarlightApiStarterBot.*.dll");
        }
    }
}
